using UnrealBuildTool;
using System.Collections.Generic;

public class BasicCxxTarget : TargetRules
{
	public BasicCxxTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "BasicCxx" } );
	}
}
